# -*- coding: utf-8 -*-
# vim: ft=sls

{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as nsd with context %}
{%- from tplroot ~ "/macros.jinja" import config_file with context %}

{%- set keys = nsd.get('keys', {}) %}

{%- if keys | length > 0 %}

{{    config_file('80-generated-keys', 'generated-keys') }}

{%- endif %}
